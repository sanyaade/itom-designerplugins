<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>Dialog2DScale</name>
    <message>
        <location filename="../dialog2DScale.ui" line="+26"/>
        <source>Interval Settings</source>
        <translation>Skaleneinstellungen</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>x-range</source>
        <translation>X-Anzeige</translation>
    </message>
    <message>
        <location line="+21"/>
        <location line="+97"/>
        <location line="+97"/>
        <source>auto</source>
        <translation>Automatisch</translation>
    </message>
    <message>
        <location line="-184"/>
        <location line="+97"/>
        <location line="+97"/>
        <source>manual adjustment:</source>
        <translation>Manuelle Ausrichtung:</translation>
    </message>
    <message>
        <location line="-179"/>
        <location line="+36"/>
        <location line="+61"/>
        <location line="+36"/>
        <location line="+87"/>
        <location line="+10"/>
        <source>0,000</source>
        <translation></translation>
    </message>
    <message>
        <location line="-217"/>
        <location line="+110"/>
        <location line="+74"/>
        <source>from</source>
        <translation>von</translation>
    </message>
    <message>
        <location line="-171"/>
        <location line="+84"/>
        <location line="+100"/>
        <source>to</source>
        <translation>bis</translation>
    </message>
    <message>
        <location line="-159"/>
        <source>y-range</source>
        <translation>Y-Anzeige</translation>
    </message>
    <message>
        <location line="+97"/>
        <source>values</source>
        <translation>Werte</translation>
    </message>
    <message>
        <source>Hint: Control the number of decimals in the spin boxes by pressing Ctrl+ or Ctrl-</source>
        <translation type="vanished">Hinweis: Um die Anzahl der Dezimalstellen in den Spinboxen zu ändern Strg+ und Strg- verwenden</translation>
    </message>
    <message>
        <location filename="../dialog2DScale.cpp" line="+205"/>
        <source>invalid number</source>
        <translation type="unfinished">Ungültige Zahl</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>The &apos;%1&apos; number is no valid decimal number.</source>
        <translation type="unfinished">Die Zahl &apos;%1&apos; ist keine gültige Dezimalzahl.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>out of range</source>
        <translation type="unfinished">Außerhalb des Gültigkeitsbereichs</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>The &apos;%1&apos; number is out of range [%2,%3]</source>
        <translation type="unfinished">Die Zahl &apos;%1&apos; liegt außerhalb des Gültigkeitsbereichs [%2, %3]</translation>
    </message>
</context>
<context>
    <name>DialogExport2File</name>
    <message>
        <location filename="../../sharedFiles/dialogExportProperties.ui" line="+14"/>
        <source>Export Properties</source>
        <translation>Export-Einstellungen</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>plot canvas size</source>
        <translation>Plotgröße</translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+108"/>
        <source>Width:</source>
        <translation>Breite:</translation>
    </message>
    <message>
        <location line="-95"/>
        <location line="+20"/>
        <source>0 px</source>
        <translation></translation>
    </message>
    <message>
        <location line="-13"/>
        <location line="+124"/>
        <source>Height:</source>
        <translation>Höhe:</translation>
    </message>
    <message>
        <location line="-101"/>
        <source>export resolution and size</source>
        <translation>Exportauflöstung und -größe</translation>
    </message>
    <message>
        <location line="+18"/>
        <location line="+146"/>
        <source> mm</source>
        <translation></translation>
    </message>
    <message>
        <location line="-115"/>
        <location line="+80"/>
        <source> px</source>
        <translation></translation>
    </message>
    <message>
        <location line="-51"/>
        <source>Format:</source>
        <translation></translation>
    </message>
    <message>
        <location line="+67"/>
        <source>Resolution:</source>
        <translation>Auflösung:</translation>
    </message>
    <message>
        <location line="+50"/>
        <source> dpi</source>
        <translation></translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Continue</source>
        <translation>Weiter</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>DialogExportProperties</name>
    <message>
        <location filename="../../sharedFiles/dialogExportProperties.cpp" line="+37"/>
        <source>user defined</source>
        <translation>Benutzerdefiniert</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>user defined (keep aspect ratio)</source>
        <translation>Benutzerdefiniert (Seitenverhältnis beibehalten)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>A4 landscape</source>
        <translation>A4 Querformat</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A4 portrait</source>
        <translation>A4 Hochformat</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A4 landscape (fitting)</source>
        <translation>A4 Querformat (angepasst)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A4 portrait (fitting)</source>
        <translation>A4 Hochformat (angepasst)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>A5 landscape</source>
        <translation>A5 Querformat</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A5 portrait</source>
        <translation>A5 Hochformat</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A5 landscape (fitting)</source>
        <translation>A5 Querformat (angepasst)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A5 portrait (fitting)</source>
        <translation>A5 Hochformat (angepasst)</translation>
    </message>
</context>
<context>
    <name>Itom2dQwtPlot</name>
    <message>
        <location filename="../itom2dqwtplot.cpp" line="+973"/>
        <location line="+12"/>
        <source>Linecut</source>
        <translation>Linienschnitt</translation>
    </message>
    <message>
        <location line="-148"/>
        <source>Could not spawn lineCut due to missing API-handle</source>
        <translation>Linienschnitt konnte nicht erzeugt werden, API-Handle wurde nicht gefunden</translation>
    </message>
    <message>
        <location line="+106"/>
        <source>the opened figure is not inherited from ito::AbstractDObjFigure</source>
        <translation>Die geöffnete Abbildung wurde nicht von ito::AbstractDObjFigure vererbt</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Z-Stack</source>
        <translation>Z-Stapel</translation>
    </message>
    <message>
        <location line="+229"/>
        <source>Set lineCut coordinates failed. Widget not ready.</source>
        <translation>Das Setzten der Linienschnittkoordinaten schlug fehl. Widget nicht bereit.</translation>
    </message>
</context>
<context>
    <name>ItomQwtDObjFigure</name>
    <message>
        <location filename="../../sharedFiles/itomQwtDObjFigure.cpp" line="+91"/>
        <source>Marker Info</source>
        <translation type="unfinished">Marker Info</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Picker Info</source>
        <translation type="unfinished">Picker Info</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Shapes Info</source>
        <translation type="unfinished">Info zu Geometische Formen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Data Object Info</source>
        <translation type="unfinished">DataObject Info</translation>
    </message>
    <message>
        <location line="+73"/>
        <location line="+40"/>
        <source>no content widget available.</source>
        <translation type="unfinished">Kein Widget für den Inhalt verfügbar.</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>%s is an unsupported file suffix. Supported values are: %s</source>
        <translation type="unfinished">&apos;%s&apos; ist eine nicht unterstützte Dateierweiterung. Unterstützte Werte sind: %s</translation>
    </message>
    <message>
        <location line="+133"/>
        <location line="+10"/>
        <source>content widget not available</source>
        <translation type="unfinished">Kein Widget für den Inhalt verfügbar</translation>
    </message>
    <message>
        <location line="+144"/>
        <location line="+12"/>
        <location line="+12"/>
        <location line="+83"/>
        <location line="+10"/>
        <source>content not available</source>
        <translation type="unfinished">Der Inhalt ist nicht verfügbar</translation>
    </message>
</context>
<context>
    <name>ItomQwtPlot</name>
    <message>
        <location filename="../../sharedFiles/itomQwtPlot.cpp" line="+239"/>
        <source>Home</source>
        <translation>Ansicht zurücksetzen</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Reset original view</source>
        <translation>Ursprüngliche Ansicht wiederherstellen</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Save...</source>
        <translation>Speichern...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Export current view...</source>
        <translation>Aktuelle Abbildung exportieren...</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Print...</source>
        <translation>Drucken...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Print preview...</source>
        <translation>Druckervorschau...</translation>
    </message>
    <message>
        <source>Copy to clipboard</source>
        <translation type="vanished">In die Zwischenablage kopieren</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Copies the current view to the clipboard</source>
        <translation type="unfinished">Aktueller Ausschnitt in die Zwischenablage kopieren</translation>
    </message>
    <message>
        <source>Send current view to workspace...</source>
        <translation type="obsolete">Aktueller Ausschnitt an Workspace senden...</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Move</source>
        <translation type="unfinished">Verschieben</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Pan axes with left mouse, zoom with right</source>
        <translation type="unfinished">Achsen verschieben mit linker Maustaste, zoomen mit rechter Maustaste</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Zoom to rectangle</source>
        <translation type="unfinished">Zoom in Rechteck</translation>
    </message>
    <message>
        <source>Clear geometric shapes</source>
        <translation type="obsolete">Geometrische Formen löschen</translation>
    </message>
    <message>
        <source>Clear all existing geometric shapes</source>
        <translation type="obsolete">Alle geometrische Formen löschen</translation>
    </message>
    <message>
        <source>Lock aspect ratio</source>
        <translation type="obsolete">Seitenverhältnis beibehalten</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Toggle fixed / variable aspect ration between axis x and y</source>
        <translation type="unfinished">Vollbild oder Seitenverhältnis beibehalten</translation>
    </message>
    <message>
        <source>Draw geometric shape</source>
        <translation type="obsolete">Geometrische Formen zeichnen</translation>
    </message>
    <message>
        <location line="-40"/>
        <source>Copy To Clipboard</source>
        <translation type="unfinished">In die Zwischenablage kopieren</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Send Current View To Workspace...</source>
        <translation type="unfinished">Aktueller Ausschnitt an Workspace senden...</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Zoom To Rectangle</source>
        <translation type="unfinished">Zoom in Rechteck</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Clear Geometric Shapes</source>
        <translation type="unfinished">Geometrische Formen löschen</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Clear All Existing Geometric Shapes</source>
        <translation type="unfinished">Alle geometrische Formen löschen</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Lock Aspect Ratio</source>
        <translation type="unfinished">Seitenverhältnis beibehalten</translation>
    </message>
    <message>
        <location line="+9"/>
        <location line="+1"/>
        <source>Draw Geometric Shape</source>
        <translation type="unfinished">Geometrische Formen zeichnen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Point</source>
        <translation>Punkt</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Line</source>
        <translation>Linie</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Rectangle</source>
        <translation>Rechteck</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Square</source>
        <translation>Quadrat</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Ellipse</source>
        <translation>Ellipse</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Circle</source>
        <translation>Kreis</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Polygon</source>
        <translation></translation>
    </message>
    <message>
        <location line="+1030"/>
        <location line="+38"/>
        <location line="+89"/>
        <location line="+84"/>
        <location line="+84"/>
        <location line="+84"/>
        <location line="+84"/>
        <location line="+84"/>
        <source>Selection has been aborted.</source>
        <translation type="unfinished">Auswahl wurde abgebrochen.</translation>
    </message>
    <message>
        <location line="-542"/>
        <location line="+38"/>
        <location line="+89"/>
        <location line="+84"/>
        <location line="+84"/>
        <location line="+84"/>
        <location line="+84"/>
        <location line="+84"/>
        <source>%1 points have been selected.</source>
        <translation type="unfinished">%1 Punkt(e) wurden ausgewählt.</translation>
    </message>
    <message>
        <location line="-460"/>
        <location line="+677"/>
        <source>Please draw %1 points. Esc aborts the selection.</source>
        <translation type="unfinished">%1 Punkte einzeichnen. ESC bricht die Auswahl ab.</translation>
    </message>
    <message>
        <location line="-676"/>
        <location line="+678"/>
        <source>Please draw one point. Esc aborts the selection.</source>
        <translation type="unfinished">Ein Punkt einzeichnen. ESC bricht die Auswahl ab.</translation>
    </message>
    <message>
        <location line="-595"/>
        <location line="+612"/>
        <source>Please draw %1 lines. Esc aborts the selection.</source>
        <translation type="unfinished">%1 Linien einzeichnen. ESC bricht die Auswahl ab.</translation>
    </message>
    <message>
        <location line="-611"/>
        <location line="+613"/>
        <source>Please draw one line. Esc aborts the selection.</source>
        <translation type="unfinished">Eine Linie einzeichnen. ESC bricht die Auswahl ab.</translation>
    </message>
    <message>
        <location line="-530"/>
        <location line="+84"/>
        <location line="+462"/>
        <source>Please draw %1 rectangles. Esc aborts the selection.</source>
        <translation type="unfinished">%1 Rechtecke einzeichnen. ESC bricht die Auswahl ab.</translation>
    </message>
    <message>
        <location line="-545"/>
        <location line="+84"/>
        <location line="+463"/>
        <source>Please draw one rectangle. Esc aborts the selection.</source>
        <translation type="unfinished">Ein Rechteck einzeichnen. ESC bricht die Auswahl ab.</translation>
    </message>
    <message>
        <location line="-380"/>
        <location line="+84"/>
        <location line="+330"/>
        <source>Please draw %1 ellipses. Esc aborts the selection.</source>
        <translation type="unfinished">%1 Ellipsen einzeichnen. ESC bricht die Auswahl ab.</translation>
    </message>
    <message>
        <location line="-413"/>
        <location line="+84"/>
        <location line="+331"/>
        <source>Please draw one ellipse. Esc aborts the selection.</source>
        <translation type="unfinished">Eine Ellipse einzeichnen. ESC bricht die Auswahl ab.</translation>
    </message>
    <message>
        <location line="-216"/>
        <source>Add points to polygon. Esc aborts the selection.</source>
        <translation type="unfinished">Punkte zum Polygon hinzufügen. &apos;ESC&apos; beendet das Hinzufügen.</translation>
    </message>
    <message>
        <location line="+74"/>
        <source>Invalid type for userInteractionStart</source>
        <translation type="unfinished">Ungültiger Typ von &apos;userInteractionStart&apos;</translation>
    </message>
    <message>
        <location line="+528"/>
        <location line="+86"/>
        <source>Invalid or unsupported shape type</source>
        <translation type="unfinished">Ungültiger oder nicht unterstützter Typ der geometrischen Form</translation>
    </message>
    <message>
        <location line="+94"/>
        <source>Invalid marker type</source>
        <translation type="unfinished">Ungültiger Marker-Typ</translation>
    </message>
    <message>
        <location line="+105"/>
        <source>The file &apos;%s&apos; cannot be created. Check the filename and the required permissions.</source>
        <translation type="unfinished">Die Datei &apos;%s&apos; kann nicht erstellt werden. Bitte den Dateinamen und die Berechtigungen prüfen.</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>Copy current view to clipboard including meta information widgets ...</source>
        <translation type="unfinished">Aktueller Ausschnitt inklusive Meta-Informations-Widget in die Zwischenablage kopieren...</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Copy current view to clipboard without meta information widgets (requires Qt5) ...</source>
        <translation type="unfinished">Aktueller Ausschnitt ohne Meta-Informations-Widget in die Zwischenablage kopieren (Qt5 erforderlich!)...</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Copy current view to clipboard ...</source>
        <translation type="unfinished">Aktueller Ausschnitt in die Zwischenablage kopieren...</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Copy current view to clipboard including infoWidgets. Done.</source>
        <translation type="unfinished">Aktueller Ausschnitt inklusive infoWidget in die Zwischenablage kopieren. Beendet.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Copy current view to clipboard. Done.</source>
        <translation type="unfinished">Aktueller Ausschnitt in die Zwischenablage kopieren. Beendet.</translation>
    </message>
    <message>
        <source>invalid type for userInteractionStart</source>
        <translation type="obsolete">Ungültiger Typ von &apos;userInteractionStart&apos;</translation>
    </message>
    <message>
        <location line="-890"/>
        <location line="+140"/>
        <source>Please select %1 points or press Space to quit earlier. Esc aborts the selection.</source>
        <translation type="unfinished">%1 Punkt(e) auswählen oder mit der Leertaste die Auswahl vorzeitig verlassen. ESC bricht die Auswahl ab.</translation>
    </message>
    <message>
        <location line="-136"/>
        <location line="+140"/>
        <source>Please select points and press Space to end the selection. Esc aborts the selection.</source>
        <translation type="unfinished">Punkte auswählen oder mit der Leertaste die Auswahl vorzeitig verlassen. ESC bricht die Auswahl ab.</translation>
    </message>
    <message>
        <location line="-63"/>
        <source>Please draw %1 squares. Esc aborts the selection.</source>
        <translation type="unfinished">%1 Quadrate einzeichnen. ESC bricht die Auswahl ab.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Please draw one square. Esc aborts the selection.</source>
        <translation type="unfinished">Ein Quadrat einzeichnen. ESC bricht die Auswahl ab.</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Please draw %1 circles. Esc aborts the selection.</source>
        <translation type="unfinished">%1 Kreise einzeichnen. ESC bricht die Auswahl ab.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Please draw one circle. Esc aborts the selection.</source>
        <translation type="unfinished">Ein Kreis einzeichnen. ESC bricht die Auswahl ab.</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Unknown type for userInteractionStart</source>
        <translation type="unfinished">Ungültiger Typ für &apos;userInteractionStart&apos;</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Selection has been interrupted.</source>
        <translation type="unfinished">Auswahl wurde abgebrochen.</translation>
    </message>
    <message>
        <location line="+120"/>
        <source>No geometric shape with index &apos;%d&apos; found.</source>
        <translation type="unfinished">Es wurde keine geometrische Form mit dem Index %d gefunden.</translation>
    </message>
    <message>
        <location line="+121"/>
        <source>Could not set active element, index out of range.</source>
        <translation type="unfinished">Der Index liegt außerhalb des erlauben Bereichs. Das aktive Element konnte nicht gesetzt werden.</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Could not set geometric shapes, api is missing</source>
        <translation type="unfinished">API wurde nicht gefunden. Die geometrische Form konnte nicht gesetzt werden</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>The shapes contain at least one shape type, that is not allowed for this plot, and is therefore ignored.</source>
        <translation type="unfinished">Die zuletzt verwendete geometrische Form ist für diesen Plot nicht erlaubt und wird deshalb ignoriert.</translation>
    </message>
    <message>
        <source>invalid or unsupported shape type</source>
        <translation type="obsolete">Ungültiger oder nicht unterstützter Typ der geometrischen Form</translation>
    </message>
    <message>
        <location line="+85"/>
        <source>Could not add a geometric shape, api is missing</source>
        <translation type="unfinished">Die geometrische Form kann nicht hinzugefügt werden, da die API nicht verfügbar ist</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Could not add the geometric shape since a shape with the same index already exists</source>
        <translation type="unfinished">Die geometrische Form kann nicht hinzugefügt werden, da bereits eine Form mit diesem Index existiert</translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+77"/>
        <source>The type of the shape is not allowed for this plot.</source>
        <translation type="unfinished">Für diesen Plot ist dieser Typ der geometrischen Formen nicht erlaubt.</translation>
    </message>
    <message>
        <location line="-4"/>
        <source>Could not modify a geometric shape, api is missing</source>
        <translation type="unfinished">Die geometrische Form kann nicht geändert  werden, da die API nicht verfügbar ist</translation>
    </message>
    <message>
        <source>invalid marker type</source>
        <translation type="obsolete">Ungültiger Marker-Typ</translation>
    </message>
    <message>
        <location line="+113"/>
        <location line="+14"/>
        <source>Geometric shape not found</source>
        <translation type="unfinished">Die geometrische Form wurde nicht gefunden</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>The file &apos;%s&apos; already exists but cannot be overwritten.</source>
        <translation type="unfinished">Die Datei &apos;%s&apos; existiert bereits, kann aber nicht überschrieben werden.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>The file &apos;%s&apos; already exists but cannot be overwritten (Maybe it is opened in another application).</source>
        <translation type="unfinished">Die Datei &apos;%s&apos; existiert bereits, kann aber nicht überschrieben werden. Evtl. ist diese Datei durch ein anderes Programm geöffnet.</translation>
    </message>
    <message>
        <source>copy current view to clipboard including meta information widgets ...</source>
        <translation type="obsolete">Aktueller Ausschnitt inklusive Meta-Informations-Widget in die Zwischenablage kopieren...</translation>
    </message>
    <message>
        <source>copy current view to clipboard without meta information widgets (requires Qt5) ...</source>
        <translation type="obsolete">Aktueller Ausschnitt ohne Meta-Informations-Widget in die Zwischenablage kopieren (Qt5 erforderlich!)...</translation>
    </message>
    <message>
        <source>copy current view to clipboard including infoWidgets ...</source>
        <translation type="obsolete">Aktuelle Ansicht inklusive infoWidget in die Zwischenablage kopieren...</translation>
    </message>
    <message>
        <source>copy current view to clipboard ...</source>
        <translation type="obsolete">Aktueller Ausschnitt in die Zwischenablage kopieren...</translation>
    </message>
    <message>
        <source>copy current view to clipboard including infoWidgets. Done.</source>
        <translation type="obsolete">Aktueller Ausschnitt inklusive infoWidget in die Zwischenablage kopieren. Beendet.</translation>
    </message>
    <message>
        <source>copy current view to clipboard. Done.</source>
        <translation type="obsolete">Aktueller Ausschnitt in die Zwischenablage kopieren. Beendet.</translation>
    </message>
    <message>
        <location line="+217"/>
        <source>PDF Documents (*.pdf)</source>
        <translation type="unfinished">FDP-Dokumente (&apos;.pdf)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>SVG Documents (*.svg)</source>
        <translation type="unfinished">SVG-Dokumente (*.svg)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Postscript Documents (*.ps)</source>
        <translation type="unfinished">Postscript-Dateien (*.ps)</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Images (</source>
        <translation type="unfinished">Bilddateien (</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Export File Name</source>
        <translation type="unfinished">Speichern unter</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Error while saving the plot</source>
        <translation type="unfinished">Fehler beim Speichern des Plots</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Warning while saving the plot</source>
        <translation type="unfinished">Beim Speichern des Plots ist eine Warnung aufgetreten</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>Could not send object to workspace, api is missing.</source>
        <translation type="unfinished">Das Objekt kann nicht in den Workspace gesendet werden. Die API wurde nicht gefunden.</translation>
    </message>
    <message>
        <source>Current to workspace</source>
        <translation type="obsolete">Aktueller Ausschnitt an den Workspace</translation>
    </message>
    <message>
        <source>Indicate the python variable name for the currently visible object</source>
        <translation type="obsolete">Variablenname für die Übernahme in den Workspace</translation>
    </message>
    <message>
        <location line="+71"/>
        <source>Error sending data object to workspace</source>
        <translation type="unfinished">Fehler beim Senden des Datenobjekts an Workspace</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Warning sending data object to workspace</source>
        <translation type="unfinished">Warnung beim Senden des Datenobjekts an Workspace</translation>
    </message>
    <message>
        <location line="+31"/>
        <location line="+1"/>
        <source>Could not plot marker, api is missing</source>
        <translation type="unfinished">Die Position kann nicht angezeigt werden, API wurde nicht gefunden</translation>
    </message>
    <message>
        <location line="+102"/>
        <source>The style tag does not correspond to the required format: ColorStyleSize[;Linewidth] (Color = b,g,r,c,m,y,k,w; Style = o,s,d,&gt;,v,^,&lt;,x,*,+,h)</source>
        <translation type="unfinished">Die Stil-Bezeichnung entspricht nicht dem erforderlichen Format: ColorStyleSize[;Linewidth] (Color = b,g,r,c,m,y,k,w; Style = o,s,d,&gt;,v,^,&lt;,x,*,+,h)</translation>
    </message>
    <message>
        <location line="+81"/>
        <source>No marker with id &apos;%1&apos; found.</source>
        <translation type="unfinished">Der Marker mit der ID &apos;%1&apos; wurde nicht gefunden.</translation>
    </message>
</context>
<context>
    <name>PlotCanvas</name>
    <message>
        <location filename="../plotCanvas.cpp" line="+1126"/>
        <source>Could not change color bar, api is missing</source>
        <translation type="unfinished">Farbleiste kann nicht geändert werden, API wurde nicht gefunden</translation>
    </message>
    <message>
        <location line="-133"/>
        <location line="+141"/>
        <source>No color maps defined.</source>
        <translation>Keine Farbpalette definiert.</translation>
    </message>
    <message>
        <location line="-926"/>
        <source>plotting tools</source>
        <translation type="unfinished">Symbolleiste Plot</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>File</source>
        <translation type="unfinished">Datei</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>View</source>
        <translation type="unfinished">Ansicht</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Tools</source>
        <translation type="unfinished">Werkzeuge</translation>
    </message>
    <message>
        <location line="+209"/>
        <source>Scale Settings...</source>
        <translation type="unfinished">Skaleneinstellungen...</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Set the ranges and offsets of this view</source>
        <translation type="unfinished">Einstellungen der Skalen</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Palette</source>
        <translation type="unfinished">Farbpalette wechseln</translation>
    </message>
    <message>
        <source>Switch between color palettes</source>
        <translation type="obsolete">Wechsel der Farbpaletten</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Click to switch to next color palette</source>
        <translation type="unfinished">Zur nächsten Farbpalette wechseln</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Color palettes</source>
        <translation type="unfinished">Farbpalette</translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+1"/>
        <source>Complex Switch</source>
        <translation type="unfinished">Komplexe Werte ein- und ausschalten</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Imag</source>
        <translation></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Real</source>
        <translation></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Abs</source>
        <translation></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Pha</source>
        <translation></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Show Colorbar</source>
        <translation type="unfinished">Farbleiste anzeigen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Toggle visibility of the color bar on the right side</source>
        <translation type="unfinished">Farbbalken auf der rechten Seite ein-/ausblenden</translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+1"/>
        <source>Data Channel</source>
        <translation type="unfinished">Datenkanal</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Auto</source>
        <translation></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Color</source>
        <translation type="unfinished">Farbe</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Gray</source>
        <translation type="unfinished">Grau</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Red</source>
        <translation type="unfinished">Rot</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Green</source>
        <translation type="unfinished">Grün</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Blue</source>
        <translation type="unfinished">Blau</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Alpha</source>
        <translation></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Marker</source>
        <translation type="unfinished">Positionsanzeiger</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show current coordinates at mouse cursor</source>
        <translation type="unfinished">Zeigt die aktuellen Koordinaten des Maus-Cursors</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Linecut</source>
        <translation type="unfinished">Linienschnitt</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show a 1D line cut (in-plane)</source>
        <translation type="unfinished">Zeigt einen 1D-Linienschnitt (auf gleicher Ebene)</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Linecut Mode</source>
        <translation type="unfinished">Linienschnittmodus</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>min &amp;&amp; max</source>
        <translation type="unfinished">Min &amp;&amp; Max</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>- &amp;&amp; min</source>
        <translation type="unfinished">- &amp;&amp; Min</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>- &amp;&amp; max</source>
        <translation type="unfinished">- &amp;&amp; Max</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>| &amp;&amp; min</source>
        <translation type="unfinished">| &amp;&amp; Min</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>| &amp;&amp; max</source>
        <translation type="unfinished">| &amp;&amp; Max</translation>
    </message>
    <message>
        <source>min &amp; max</source>
        <translation type="obsolete">Min &amp; Max</translation>
    </message>
    <message>
        <location line="-19"/>
        <source>line cut through global minimum and maximum value</source>
        <translation type="unfinished">Globales Minimum und Maximum für Linienschnitt übernehmen</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>horizontal line cut through global minimum value</source>
        <translation type="unfinished">Globales Minimum für horizontalen Linienschnitt übernehmen</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>horizontal line cut through global maximum value</source>
        <translation type="unfinished">Globales Maximum für horizontalen Linienschnitt übernehmen</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>vertical line cut through global minimum value</source>
        <translation type="unfinished">Globales Minimum für vertikalen Linienschnitt übernehmen</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>vertical line cut through global maximum value</source>
        <translation type="unfinished">Globales Maximum für vertikalen Linienschnitt übernehmen</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Control alpha-value of overlay image</source>
        <translation type="unfinished">Steuerung für den Alpa-Wert des Overlay-Bilds</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Set alpha for overlay</source>
        <translation type="unfinished">Alpha für das Overlay setzen</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Slice in z-direction</source>
        <translation type="unfinished">Schnitt in Z-Richtung</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Show a slice through z-stack</source>
        <translation type="unfinished">Zeigt einen Schnitt in Z-Richtung</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Center marker</source>
        <translation type="unfinished">Mittelpunktsanzeige</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Display a cross at data object&apos;s center</source>
        <translation type="unfinished">Blendet in der Mitte des DataObjects ein Kreuz ein</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Select image plane</source>
        <translation type="unfinished">Bildebene auswählen</translation>
    </message>
    <message>
        <location line="+416"/>
        <location line="+131"/>
        <source>Selected color map has less than two points.</source>
        <translation>Ausgewählte Farbpalette hat weniger als zwei Punkte.</translation>
    </message>
    <message>
        <location line="+1184"/>
        <source>Set lineCut coordinates failed. Could not activate lineCut.</source>
        <translation type="unfinished">Das Setzten der Linienschnittkoordinaten schlug fehl. Der Linienschnitt konnte nicht aktiviert werden.</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../itom2dqwtplot.cpp" line="-1115"/>
        <source>Points for line plots from 2d objects</source>
        <translation type="unfinished">Punkte für Linien-Plots in 2D-Objekten</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Points for z-stack cut in 3d objects</source>
        <translation type="unfinished">Punkte für Z-Stapelschnitte in 3D-Objekten</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>shallow copy of input source object</source>
        <translation>Referenz auf Eingangsobjekt</translation>
    </message>
    <message>
        <location filename="../plotCanvas.cpp" line="-2079"/>
        <source>plot2D</source>
        <translation type="unfinished">2D-Plot</translation>
    </message>
    <message>
        <location filename="../itom2dqwtplotplugin.cpp" line="+38"/>
        <source>itom widget for 2D-visualisation of 2D/3D dataObjects based on Qwt.</source>
        <translation type="unfinished">Itom-Widget für die 2D-Visualisierung von 2D/3D-Datenobjekten basierend auf Qwt.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This designer plugin is an itom widget for image-like visualisation of dataObjects and live images. This widget is based on the Qwt framework (http://qwt.sf.net).</source>
        <translation type="unfinished">Dieses Designer-Plugin ist ein itom Widget zur bildgleichen Visualisierung von DataObjects und Live-Anzeige von Linienkamera. Das Widget basiert auf Qwt framework (http://qwt.sf.net).</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>LGPL, for Qwt see Qwt License</source>
        <translation>LGPL, für Qwt siehe Qwt-Lizenz</translation>
    </message>
    <message>
        <source>rotated shapes are currently not supported.</source>
        <translation type="obsolete">Das Rotieren der geometischen Formen wird derzeit nicht unterstützt.</translation>
    </message>
    <message>
        <location filename="../../sharedFiles/drawItem.cpp" line="+1052"/>
        <source>invalid geometric shape type</source>
        <translation type="unfinished">Ungültige Typ der geometischen Formen</translation>
    </message>
</context>
</TS>
