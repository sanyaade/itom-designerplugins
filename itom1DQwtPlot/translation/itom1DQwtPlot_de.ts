<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>Dialog1DScale</name>
    <message>
        <location filename="../dialog1DScale.ui" line="+26"/>
        <source>Interval Settings</source>
        <translation>Skaleneinstellungen</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>x-range</source>
        <translation>X-Ansicht</translation>
    </message>
    <message>
        <location line="+21"/>
        <location line="+103"/>
        <source>auto</source>
        <translation>Automatisch</translation>
    </message>
    <message>
        <location line="-93"/>
        <location line="+103"/>
        <source>manual adjustment:</source>
        <translation>Manuelle Ausrichtung:</translation>
    </message>
    <message>
        <location line="-69"/>
        <location line="+113"/>
        <source>from</source>
        <translation>von</translation>
    </message>
    <message>
        <source>Hint: Control the number of decimals in the spin boxes by pressing Ctrl+ or Ctrl-</source>
        <translation type="vanished">Hinweis: Um die Anzahl der Dezimalstellen in den Spinboxen zu ändern Strg+ und Strg- verwenden</translation>
    </message>
    <message>
        <location line="-129"/>
        <location line="+113"/>
        <source>to</source>
        <translation>bis</translation>
    </message>
    <message>
        <location line="-84"/>
        <location line="+10"/>
        <location line="+61"/>
        <location line="+42"/>
        <source>0,000</source>
        <translation></translation>
    </message>
    <message>
        <location line="-88"/>
        <source>y-range</source>
        <translation>Y-Ansicht</translation>
    </message>
    <message>
        <location filename="../dialog1DScale.cpp" line="+173"/>
        <source>invalid number</source>
        <translation type="unfinished">Ungültige Zahl</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>The &apos;%1&apos; number is no valid decimal number.</source>
        <translation type="unfinished">Die Zahl &apos;%1&apos; ist keine gültige Dezimalzahl.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>out of range</source>
        <translation type="unfinished">Außerhalb des Gültigkeitsbereichs</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>The &apos;%1&apos; number is out of range [%2,%3]</source>
        <translation type="unfinished">Die Zahl &apos;%1&apos; liegt außerhalb des Gültigkeitsbereichs [%2, %3]</translation>
    </message>
</context>
<context>
    <name>DialogExport2File</name>
    <message>
        <location filename="../../sharedFiles/dialogExportProperties.ui" line="+14"/>
        <source>Export Properties</source>
        <translation>Export-Einstellungen</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>plot canvas size</source>
        <translation>Plotgröße</translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+108"/>
        <source>Width:</source>
        <translation>Breite:</translation>
    </message>
    <message>
        <location line="-95"/>
        <location line="+20"/>
        <source>0 px</source>
        <translation></translation>
    </message>
    <message>
        <location line="-13"/>
        <location line="+124"/>
        <source>Height:</source>
        <translation>Höhe:</translation>
    </message>
    <message>
        <location line="-101"/>
        <source>export resolution and size</source>
        <translation>Exportauflöstung und -größe</translation>
    </message>
    <message>
        <location line="+18"/>
        <location line="+146"/>
        <source> mm</source>
        <translation></translation>
    </message>
    <message>
        <location line="-115"/>
        <location line="+80"/>
        <source> px</source>
        <translation></translation>
    </message>
    <message>
        <location line="-51"/>
        <source>Format:</source>
        <translation></translation>
    </message>
    <message>
        <location line="+67"/>
        <source>Resolution:</source>
        <translation>Auflösung:</translation>
    </message>
    <message>
        <location line="+50"/>
        <source> dpi</source>
        <translation></translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Continue</source>
        <translation>Weiter</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>DialogExportProperties</name>
    <message>
        <location filename="../../sharedFiles/dialogExportProperties.cpp" line="+37"/>
        <source>user defined</source>
        <translation>Benutzerdefiniert</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>user defined (keep aspect ratio)</source>
        <translation>Benutzerdefiniert (Seitenverhältnis beibehalten)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>A4 landscape</source>
        <translation>A4 Querformat</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A4 portrait</source>
        <translation>A4 Hochformat</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A4 landscape (fitting)</source>
        <translation>A4 Querformat (angepasst)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A4 portrait (fitting)</source>
        <translation>A4 Hochformat (angepasst)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>A5 landscape</source>
        <translation>A5 Querformat</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A5 portrait</source>
        <translation>A5 Hochformat</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A5 landscape (fitting)</source>
        <translation>A5 Querformat (angepasst)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A5 portrait (fitting)</source>
        <translation>A5 Hochformat (angepasst)</translation>
    </message>
</context>
<context>
    <name>Itom1DQwtPlot</name>
    <message>
        <location filename="../itom1DQwtPlot.cpp" line="+62"/>
        <source>Points for line plots from 2d objects</source>
        <translation>Punkte für Linien-Plots von 2D-Objekten</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>represents the xData of the given source object</source>
        <translation type="unfinished">Repräsentiert &apos;xData&apos; des Quellobjekts</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Curve Properties</source>
        <translation>Kurveneigenschaften</translation>
    </message>
    <message>
        <location line="+760"/>
        <location line="+10"/>
        <source>Set picker failed, since canvas not initilized</source>
        <translation>Der Picker konnte nicht gesetzt werden, da &apos;Canvas&apos; nicht inizialisiert wurde</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Delete picker failed, since canvas not initilized</source>
        <translation>Der Picker konnte nicht gelöscht werden, da &apos;Canvas&apos; nicht inizialisiert wurde</translation>
    </message>
    <message>
        <location line="+10"/>
        <location line="+10"/>
        <source>Get picker failed, canvas handle not initilized.</source>
        <translation>Fehler: &apos;Canvas-Handle&apos; wurde nicht initialisiert.</translation>
    </message>
    <message>
        <location line="+201"/>
        <source>canvas not available</source>
        <translation>&apos;Canvas&apos; ist nicht verfügbar</translation>
    </message>
</context>
<context>
    <name>ItomQwtDObjFigure</name>
    <message>
        <location filename="../../sharedFiles/itomQwtDObjFigure.cpp" line="+91"/>
        <source>Marker Info</source>
        <translation type="unfinished">Marker Info</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Picker Info</source>
        <translation type="unfinished">Picker Info</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Shapes Info</source>
        <translation type="unfinished">Info zu Geometische Formen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Data Object Info</source>
        <translation type="unfinished">DataObject Info</translation>
    </message>
    <message>
        <location line="+73"/>
        <location line="+40"/>
        <source>no content widget available.</source>
        <translation type="unfinished">Kein Widget für den Inhalt verfügbar.</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>%s is an unsupported file suffix. Supported values are: %s</source>
        <translation type="unfinished">&apos;%s&apos; ist eine nicht unterstützte Dateierweiterung. Unterstützte Werte sind: %s</translation>
    </message>
    <message>
        <location line="+133"/>
        <location line="+10"/>
        <source>content widget not available</source>
        <translation type="unfinished">Kein Widget für den Inhalt verfügbar</translation>
    </message>
    <message>
        <location line="+144"/>
        <location line="+12"/>
        <location line="+12"/>
        <location line="+83"/>
        <location line="+10"/>
        <source>content not available</source>
        <translation type="unfinished">Der Inhalt ist nicht verfügbar</translation>
    </message>
</context>
<context>
    <name>ItomQwtPlot</name>
    <message>
        <location filename="../../sharedFiles/itomQwtPlot.cpp" line="+239"/>
        <source>Home</source>
        <translation>Ansicht zurücksetzen</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Reset original view</source>
        <translation>Ursprüngliche Ansicht wiederherstellen</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Save...</source>
        <translation>Speichern...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Export current view...</source>
        <translation>Aktuelle Abbildung exportieren...</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Print...</source>
        <translation>Drucken...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Print preview...</source>
        <translation>Druckervorschau...</translation>
    </message>
    <message>
        <source>Copy to clipboard</source>
        <translation type="vanished">In die Zwischenablage kopieren</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Copies the current view to the clipboard</source>
        <translation type="unfinished">Aktueller Ausschnitt in die Zwischenablage kopieren</translation>
    </message>
    <message>
        <source>Send current view to workspace...</source>
        <translation type="obsolete">Aktueller Ausschnitt an Workspace senden...</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Move</source>
        <translation type="unfinished">Verschieben</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Pan axes with left mouse, zoom with right</source>
        <translation type="unfinished">Achsen verschieben mit linker Maustaste, vergrößern mit rechter Maustaste</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Zoom to rectangle</source>
        <translation type="unfinished">Zoom in Rechteck</translation>
    </message>
    <message>
        <source>Clear geometric shapes</source>
        <translation type="obsolete">Geometrische Formen löschen</translation>
    </message>
    <message>
        <source>Clear all existing geometric shapes</source>
        <translation type="obsolete">Alle geometrische Formen löschen</translation>
    </message>
    <message>
        <source>Lock aspect ratio</source>
        <translation type="obsolete">Bildformat anzeigen</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Toggle fixed / variable aspect ration between axis x and y</source>
        <translation type="unfinished">Vollbild oder Seitenverhältnis beibehalten</translation>
    </message>
    <message>
        <source>Draw geometric shape</source>
        <translation type="obsolete">Geometrische Formen zeichnen</translation>
    </message>
    <message>
        <location line="-40"/>
        <source>Copy To Clipboard</source>
        <translation type="unfinished">In die Zwischenablage kopieren</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Send Current View To Workspace...</source>
        <translation type="unfinished">Aktueller Ausschnitt an Workspace senden...</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Zoom To Rectangle</source>
        <translation type="unfinished">Zoom in Rechteck</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Clear Geometric Shapes</source>
        <translation type="unfinished">Geometrische Formen löschen</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Clear All Existing Geometric Shapes</source>
        <translation type="unfinished">Clear all existing geometric shapes</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Lock Aspect Ratio</source>
        <translation type="unfinished">Bildformat anzeigen</translation>
    </message>
    <message>
        <location line="+9"/>
        <location line="+1"/>
        <source>Draw Geometric Shape</source>
        <translation type="unfinished">Geometrische Formen zeichnen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Point</source>
        <translation>Punkt</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Line</source>
        <translation>Linie</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Rectangle</source>
        <translation>Rechteck</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Square</source>
        <translation>Quadrat</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Ellipse</source>
        <translation>Ellipse</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Circle</source>
        <translation>Kreis</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Polygon</source>
        <translation></translation>
    </message>
    <message>
        <location line="+1030"/>
        <location line="+38"/>
        <location line="+89"/>
        <location line="+84"/>
        <location line="+84"/>
        <location line="+84"/>
        <location line="+84"/>
        <location line="+84"/>
        <source>Selection has been aborted.</source>
        <translation type="unfinished">Auswahl wurde abgebrochen.</translation>
    </message>
    <message>
        <location line="-542"/>
        <location line="+38"/>
        <location line="+89"/>
        <location line="+84"/>
        <location line="+84"/>
        <location line="+84"/>
        <location line="+84"/>
        <location line="+84"/>
        <source>%1 points have been selected.</source>
        <translation type="unfinished">%1 Punkt(e) wurden ausgewählt.</translation>
    </message>
    <message>
        <location line="-460"/>
        <location line="+677"/>
        <source>Please draw %1 points. Esc aborts the selection.</source>
        <translation type="unfinished">%1 Punkte einzeichnen. ESC bricht die Auswahl ab.</translation>
    </message>
    <message>
        <location line="-676"/>
        <location line="+678"/>
        <source>Please draw one point. Esc aborts the selection.</source>
        <translation type="unfinished">Ein Punkt einzeichnen. ESC bricht die Auswahl ab.</translation>
    </message>
    <message>
        <location line="-595"/>
        <location line="+612"/>
        <source>Please draw %1 lines. Esc aborts the selection.</source>
        <translation type="unfinished">%1 Linien einzeichnen. ESC bricht die Auswahl ab.</translation>
    </message>
    <message>
        <location line="-611"/>
        <location line="+613"/>
        <source>Please draw one line. Esc aborts the selection.</source>
        <translation type="unfinished">Eine Linie einzeichnen. ESC bricht die Auswahl ab.</translation>
    </message>
    <message>
        <location line="-530"/>
        <location line="+84"/>
        <location line="+462"/>
        <source>Please draw %1 rectangles. Esc aborts the selection.</source>
        <translation type="unfinished">%1 Rechtecke einzeichnen. ESC bricht die Auswahl ab.</translation>
    </message>
    <message>
        <location line="-545"/>
        <location line="+84"/>
        <location line="+463"/>
        <source>Please draw one rectangle. Esc aborts the selection.</source>
        <translation type="unfinished">Ein Rechteck einzeichnen. ESC bricht die Auswahl ab.</translation>
    </message>
    <message>
        <location line="-380"/>
        <location line="+84"/>
        <location line="+330"/>
        <source>Please draw %1 ellipses. Esc aborts the selection.</source>
        <translation type="unfinished">%1 Ellipsen einzeichnen. ESC bricht die Auswahl ab.</translation>
    </message>
    <message>
        <location line="-413"/>
        <location line="+84"/>
        <location line="+331"/>
        <source>Please draw one ellipse. Esc aborts the selection.</source>
        <translation type="unfinished">Eine Ellipse einzeichnen. ESC bricht die Auswahl ab.</translation>
    </message>
    <message>
        <location line="-216"/>
        <source>Add points to polygon. Esc aborts the selection.</source>
        <translation type="unfinished">Punkte zum Polygon hinzufügen. &apos;ESC&apos; beendet das Hinzufügen.</translation>
    </message>
    <message>
        <location line="+74"/>
        <source>Invalid type for userInteractionStart</source>
        <translation type="unfinished">invalid type for userInteractionStart</translation>
    </message>
    <message>
        <location line="+528"/>
        <location line="+86"/>
        <source>Invalid or unsupported shape type</source>
        <translation type="unfinished">Ungültiger oder nicht unterstützter Typ der geometrischen Form</translation>
    </message>
    <message>
        <location line="+94"/>
        <source>Invalid marker type</source>
        <translation type="unfinished">Ungültiger Marker-Typ</translation>
    </message>
    <message>
        <location line="+105"/>
        <source>The file &apos;%s&apos; cannot be created. Check the filename and the required permissions.</source>
        <translation type="unfinished">Die Datei &apos;%s&apos; kann nicht erstellt werden. Bitte den Dateinamen und die Berechtigungen prüfen.</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>Copy current view to clipboard including meta information widgets ...</source>
        <translation type="unfinished">Aktueller Ausschnitt inklusive Meta-Informations-Widget in die Zwischenablage kopieren...</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Copy current view to clipboard without meta information widgets (requires Qt5) ...</source>
        <translation type="unfinished">Aktueller Ausschnitt ohne Meta-Informations-Widget in die Zwischenablage kopieren (Qt5 erforderlich!)...</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Copy current view to clipboard ...</source>
        <translation type="unfinished">Aktueller Ausschnitt in die Zwischenablage kopieren...</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Copy current view to clipboard including infoWidgets. Done.</source>
        <translation type="unfinished">Aktueller Ausschnitt inklusive infoWidget in die Zwischenablage kopieren. Beendet.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Copy current view to clipboard. Done.</source>
        <translation type="unfinished">Aktueller Ausschnittt in die Zwischenablage kopieren. Beendet.</translation>
    </message>
    <message>
        <source>invalid type for userInteractionStart</source>
        <translation type="obsolete">Ungültiger Typ von &apos;userInteractionStart&apos;</translation>
    </message>
    <message>
        <location line="-890"/>
        <location line="+140"/>
        <source>Please select %1 points or press Space to quit earlier. Esc aborts the selection.</source>
        <translation type="unfinished">%1 Punkt(e) auswählen oder mit der Leertaste die Auswahl vorzeitig verlassen. ESC bricht die Auswahl ab.</translation>
    </message>
    <message>
        <location line="-136"/>
        <location line="+140"/>
        <source>Please select points and press Space to end the selection. Esc aborts the selection.</source>
        <translation type="unfinished">Punkte auswählen oder mit der Leertaste die Auswahl vorzeitig verlassen. ESC bricht die Auswahl ab.</translation>
    </message>
    <message>
        <location line="-63"/>
        <source>Please draw %1 squares. Esc aborts the selection.</source>
        <translation type="unfinished">%1 Quadrate einzeichnen. ESC bricht die Auswahl ab.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Please draw one square. Esc aborts the selection.</source>
        <translation type="unfinished">Ein Quadrat einzeichnen. ESC bricht die Auswahl ab.</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Please draw %1 circles. Esc aborts the selection.</source>
        <translation type="unfinished">%1 Kreise einzeichnen. ESC bricht die Auswahl ab.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Please draw one circle. Esc aborts the selection.</source>
        <translation type="unfinished">Ein Kreis einzeichnen. ESC bricht die Auswahl ab.</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Unknown type for userInteractionStart</source>
        <translation type="unfinished">Unbekannter Typ als &quot;userInteractionStart&quot;</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Selection has been interrupted.</source>
        <translation type="unfinished">Auswahl wurde abgebrochen.</translation>
    </message>
    <message>
        <location line="+120"/>
        <source>No geometric shape with index &apos;%d&apos; found.</source>
        <translation type="unfinished">Es wurde keine geometrische Form mit dem Index %d gefunden.</translation>
    </message>
    <message>
        <location line="+121"/>
        <source>Could not set active element, index out of range.</source>
        <translation type="unfinished">Der Index liegt außerhalb des erlauben Bereichs. Das aktive Element konnte nicht gesetzt werden.</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Could not set geometric shapes, api is missing</source>
        <translation type="unfinished">API wurde nicht gefunden. Die geometrische Form konnte nicht gesetzt werden</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>The shapes contain at least one shape type, that is not allowed for this plot, and is therefore ignored.</source>
        <translation type="unfinished">Die zuletzt verwendete geometrische Form ist für diesen Plot nicht erlaubt und wird deshalb ignoriert.</translation>
    </message>
    <message>
        <source>invalid or unsupported shape type</source>
        <translation type="obsolete">Ungültiger oder nicht unterstützter Typ der geometrischen Form</translation>
    </message>
    <message>
        <location line="+85"/>
        <source>Could not add a geometric shape, api is missing</source>
        <translation type="unfinished">Die geometrische Form kann nicht hinzugefügt werden, da die API nicht verfügbar ist</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Could not add the geometric shape since a shape with the same index already exists</source>
        <translation type="unfinished">Die geometrische Form kann nicht hinzugefügt werden, da bereits eine Form mit diesem Index existiert</translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+77"/>
        <source>The type of the shape is not allowed for this plot.</source>
        <translation type="unfinished">Für diesen Plot ist dieser Typ der geometrischen Formen nicht erlaubt.</translation>
    </message>
    <message>
        <location line="-4"/>
        <source>Could not modify a geometric shape, api is missing</source>
        <translation type="unfinished">Die geometrische Form kann nicht geändert  werden, da die API nicht verfügbar ist</translation>
    </message>
    <message>
        <source>invalid marker type</source>
        <translation type="obsolete">Ungültiger Marker-Typ</translation>
    </message>
    <message>
        <location line="+113"/>
        <location line="+14"/>
        <source>Geometric shape not found</source>
        <translation type="unfinished">Die geometrische Form wurde nicht gefunden</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>The file &apos;%s&apos; already exists but cannot be overwritten.</source>
        <translation type="unfinished">Die Datei &apos;%s&apos; existiert bereits, kann aber nicht überschrieben werden.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>The file &apos;%s&apos; already exists but cannot be overwritten (Maybe it is opened in another application).</source>
        <translation type="unfinished">Die Datei &apos;%s&apos; existiert bereits, kann aber nicht überschrieben werden. Evtl. ist diese Datei durch ein anderes Programm geöffnet.</translation>
    </message>
    <message>
        <source>copy current view to clipboard including meta information widgets ...</source>
        <translation type="obsolete">Aktueller Ausschnitt inklusive Meta-Informations-Widget in die Zwischenablage kopieren...</translation>
    </message>
    <message>
        <source>copy current view to clipboard without meta information widgets (requires Qt5) ...</source>
        <translation type="obsolete">Aktueller Ausschnitt ohne Meta-Informations-Widget in die Zwischenablage kopieren (Qt5 erforderlich!)...</translation>
    </message>
    <message>
        <source>copy current view to clipboard including infoWidgets ...</source>
        <translation type="obsolete">Aktuelle Ansicht inklusive infoWidget in die Zwischenablage kopieren...</translation>
    </message>
    <message>
        <source>copy current view to clipboard ...</source>
        <translation type="obsolete">Aktueller Ausschnitt in die Zwischenablage kopieren...</translation>
    </message>
    <message>
        <source>copy current view to clipboard including infoWidgets. Done.</source>
        <translation type="obsolete">Aktueller Ausschnitt inklusive infoWidget in die Zwischenablage kopieren. Beendet.</translation>
    </message>
    <message>
        <source>copy current view to clipboard. Done.</source>
        <translation type="obsolete">Aktueller Ausschnittt in die Zwischenablage kopieren. Beendet.</translation>
    </message>
    <message>
        <location line="+217"/>
        <source>PDF Documents (*.pdf)</source>
        <translation type="unfinished">FDP-Dokumente (&apos;.pdf)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>SVG Documents (*.svg)</source>
        <translation type="unfinished">SVG-Dokumente (*.svg)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Postscript Documents (*.ps)</source>
        <translation type="unfinished">Postscript-Dateien (*.ps)</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Images (</source>
        <translation type="unfinished">Bilddateien (</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Export File Name</source>
        <translation type="unfinished">Speichern unter</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Error while saving the plot</source>
        <translation type="unfinished">Fehler beim Speichern des Plots</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Warning while saving the plot</source>
        <translation type="unfinished">Beim Speichern des Plots ist eine Warnung aufgetreten</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>Could not send object to workspace, api is missing.</source>
        <translation type="unfinished">Das Objekt kann nicht in den Workspace gesendet werden. Die API wurde nicht gefunden.</translation>
    </message>
    <message>
        <source>Current to workspace</source>
        <translation type="obsolete">Aktueller Ausschnitt an den Workspace</translation>
    </message>
    <message>
        <source>Indicate the python variable name for the currently visible object</source>
        <translation type="obsolete">Variablenname für die Übernahme in den Workspace</translation>
    </message>
    <message>
        <location line="+71"/>
        <source>Error sending data object to workspace</source>
        <translation type="unfinished">Fehler beim Senden des Datenobjekts an Workspace</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Warning sending data object to workspace</source>
        <translation type="unfinished">Warnung beim Senden des Datenobjekts an Workspace</translation>
    </message>
    <message>
        <location line="+31"/>
        <location line="+1"/>
        <source>Could not plot marker, api is missing</source>
        <translation type="unfinished">Der Marker konnte nicht gezeichnet werden weil API fehlt</translation>
    </message>
    <message>
        <location line="+102"/>
        <source>The style tag does not correspond to the required format: ColorStyleSize[;Linewidth] (Color = b,g,r,c,m,y,k,w; Style = o,s,d,&gt;,v,^,&lt;,x,*,+,h)</source>
        <translation type="unfinished">Die Stil-Bezeichnung entspricht nicht dem erforderlichen Format: ColorStyleSize[;Linewidth] (Color = b,g,r,c,m,y,k,w; Style = o,s,d,&gt;,v,^,&lt;,x,*,+,h)</translation>
    </message>
    <message>
        <location line="+81"/>
        <source>No marker with id &apos;%1&apos; found.</source>
        <translation type="unfinished">Es wurde kein Marker mit der ID %1 gefunden.</translation>
    </message>
</context>
<context>
    <name>Plot1DWidget</name>
    <message>
        <source>plotting tools</source>
        <translation type="obsolete">Symbolleiste</translation>
    </message>
    <message>
        <location filename="../plot1DWidget.cpp" line="+215"/>
        <source>File</source>
        <translation type="unfinished">Datei</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>View</source>
        <translation>Ansicht</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Curve Properties</source>
        <translation type="unfinished">Kurveneigenschaften</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Tools</source>
        <translation>Werkzeuge</translation>
    </message>
    <message>
        <location line="+174"/>
        <source>Scale Settings...</source>
        <translation>Skaleneinstellungen...</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Set the ranges and offsets of this view</source>
        <translation type="unfinished">Einstellungen der Skalen</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Parent Scale Settings</source>
        <translation type="unfinished">Skaleneinstellungen Ursprungsabbildung</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Set the value-range of the parent view according to this plot</source>
        <translation type="unfinished">Skaleneinstellungen auch für die ursprüngliche Abbildung übernehmen</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Forward</source>
        <translation type="unfinished">Vorwärts</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Forward to next line</source>
        <translation type="unfinished">Zur nächsten Linie wechseln</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Back</source>
        <translation>Zurück</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Back to previous line</source>
        <translation>Zur vorherigen Linie zurück</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Picker</source>
        <translation></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Set Pickers...</source>
        <translation>Picker setzen...</translation>
    </message>
    <message>
        <source>To Min-Max</source>
        <translation type="vanished">Auf Min/Max</translation>
    </message>
    <message>
        <source>set two pickers to absolute minimum and maximum of (first) curve</source>
        <translation type="obsolete">Setzt zwei Picker für das absolute &apos;Minimum und Maximim der (ersten) Kurve</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Delete Pickers</source>
        <translation>Picker löschen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>delete all pickers</source>
        <translation>Alle Picker löschen</translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+1"/>
        <source>Complex Switch</source>
        <translation type="unfinished">Komplex-Schalter</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Imag</source>
        <translation></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Real</source>
        <translation></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Abs</source>
        <translation></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Pha</source>
        <translation></translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+2"/>
        <source>Legend</source>
        <translation>Legende</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Off</source>
        <translation>Aus</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Right</source>
        <translation>Rechts</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Bottom</source>
        <translation>Unten</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Left</source>
        <translation>Links</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Top</source>
        <translation>Oben</translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+1"/>
        <source>Data Representation</source>
        <translation>Darstellung</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Auto</source>
        <translation></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>First Row</source>
        <translation type="unfinished">Erste Zeile</translation>
    </message>
    <message>
        <location line="+2514"/>
        <source>CurveIndex out of bounds [0,%i]</source>
        <translation type="unfinished">Der &apos;curveIndex&apos; liegt außerhalb des Gültigkeitsbereichs [0, %i]</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Number of new pickers exceed the given picker limit of %i</source>
        <translation type="unfinished">Die Anzahl neuer Picker überschreitet das Limit für Picker von %i</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Number of pickers exceed the given picker limit of %i</source>
        <translation type="unfinished">Die Anzahl der Picker überschreitet das Limit für Picker von %i</translation>
    </message>
    <message>
        <location line="+81"/>
        <source>ID out of range [0,%i]</source>
        <translation type="unfinished">Die ID liegt außerhalb des Gültigkeitsbereichs [0, %i]</translation>
    </message>
    <message>
        <location line="+693"/>
        <source>Select a visible curve</source>
        <translation type="unfinished">Eine sichbare Kurve auswählen</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Select the curve the picker values should be referred to</source>
        <translation type="unfinished">Die Kurve auswählen, mit der der Picker-Wert verknüpft werden soll</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>Index out of bounds [0,%i]</source>
        <translation type="unfinished">Der Index liegt außerhalb des Gültigkeitsbereichs [0, %i]</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Properties of curve %i are not available.</source>
        <translation type="unfinished">Die Eigenschaften der Kurve %i ist nicht verfügbar.</translation>
    </message>
    <message>
        <source>first row</source>
        <translation type="obsolete">Erste Zeile</translation>
    </message>
    <message>
        <source>first column</source>
        <translation type="obsolete">Erste Spalte</translation>
    </message>
    <message>
        <source>multi row</source>
        <translation type="obsolete">Alle Zeilen</translation>
    </message>
    <message>
        <source>multi column</source>
        <translation type="obsolete">Alle Spalten</translation>
    </message>
    <message>
        <source>multi layer</source>
        <translation type="obsolete">Alle Schichten</translation>
    </message>
    <message>
        <location line="-3332"/>
        <location line="+1"/>
        <source>Color Representation</source>
        <translation type="unfinished">Farbdarstellung</translation>
    </message>
    <message>
        <source>auto value</source>
        <translation type="obsolete">Autowert</translation>
    </message>
    <message>
        <source>gray value</source>
        <translation type="obsolete">Grauwert</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>RGB-lines</source>
        <translation type="unfinished">RGB-Linien</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>RGBA-lines</source>
        <translation type="unfinished">RGBA-Linien</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>RGB + Gray</source>
        <translation type="unfinished">RGB + Grau</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Marker Positions</source>
        <translation type="unfinished">Positionsanzeiger</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Marker Offsets</source>
        <translation type="unfinished">Offset Anzeige</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Grid</source>
        <translation type="unfinished">Gitter</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Shows/hides a grid</source>
        <translation type="unfinished">Gitter ein-/ausblenden</translation>
    </message>
    <message>
        <location line="+659"/>
        <location line="+4"/>
        <location line="+4"/>
        <location line="+5"/>
        <source>blue</source>
        <translation type="unfinished">blau</translation>
    </message>
    <message>
        <location line="-13"/>
        <location line="+4"/>
        <location line="+4"/>
        <location line="+5"/>
        <source>green</source>
        <translation type="unfinished">grün</translation>
    </message>
    <message>
        <location line="-13"/>
        <location line="+4"/>
        <location line="+4"/>
        <location line="+5"/>
        <source>red</source>
        <translation type="unfinished">rot</translation>
    </message>
    <message>
        <source>curveIndex out of bounds [0,%i]</source>
        <translation type="obsolete">Der &apos;curveIndex&apos; liegt außerhalb des Gültigkeitsbereichs [0, %i]</translation>
    </message>
    <message>
        <source>number of new pickers exceed the given picker limit of %i</source>
        <translation type="obsolete">Die Anzahl neuer Picker überschreitet das Limit für Picker von %i</translation>
    </message>
    <message>
        <source>number of pickers exceed the given picker limit of %i</source>
        <translation type="obsolete">Die Anzahl der Picker überschreitet das Limit für Picker von %i</translation>
    </message>
    <message>
        <source>id out of range [0,%i]</source>
        <translation type="obsolete">Die ID liegt außerhalb des Gültigkeitsbereichs [0, %i]</translation>
    </message>
    <message>
        <source>index out of bounds [0,%i]</source>
        <translation type="obsolete">Der Index liegt außerhalb des Gültigkeitsbereichs [0, %i]</translation>
    </message>
    <message>
        <source>properties of curve %i are not available.</source>
        <translation type="obsolete">Die Eigenschaften der Kurve %i ist nicht verfügbar.</translation>
    </message>
    <message>
        <location line="+2635"/>
        <source>itom API not available.</source>
        <translation type="unfinished">Die itom-API ist nicht verfügbar.</translation>
    </message>
    <message>
        <location line="-2937"/>
        <location line="+1"/>
        <location line="+11"/>
        <location line="+1"/>
        <location line="+410"/>
        <location line="+1"/>
        <source>curve %1</source>
        <translation>Kurve %1</translation>
    </message>
    <message>
        <location line="-1192"/>
        <source>Plotting Tools</source>
        <translation type="unfinished">Symbolleiste</translation>
    </message>
    <message>
        <location line="+277"/>
        <source>To Global Min-Max</source>
        <translation type="unfinished">Zum globalen Min/Max</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>set two pickers to the absolute minimum and maximum of the curve. 
If multiple curves are visible, the user can select the appropriate one.</source>
        <translation type="unfinished">Setzt zwei Picker für das absolute &apos;Minimum und Maximim der Kurve.
Sind mehrere Kurven sichtbar, kann der Benutzer die Passende auswählen.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>To Min-Max In Current View</source>
        <translation type="unfinished">Zum Min/Max in aktueller Ansicht</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>set two pickers to the absolute minimum and maximum of the curve (within the current view). 
If multiple curves are visible, the user can select the appropriate one.</source>
        <translation type="unfinished">Setzt zwei Picker für das absolute &apos;Minimum und Maximim der Kurve. (in der aktuellen Ansicht).
Sind mehrere Kurven sichtbar, kann der Benutzer die Passende auswählen.</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>First Column</source>
        <translation type="unfinished">Erste Spalte</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Multi Row</source>
        <translation type="unfinished">Alle Zeilen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Multi Column</source>
        <translation type="unfinished">Alle Spalten</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Multi Layer</source>
        <translation type="unfinished">Alle Schichten</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Auto Value</source>
        <translation type="unfinished">Autowert</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Gray Value</source>
        <translation type="unfinished">Grauwert</translation>
    </message>
    <message>
        <location line="+685"/>
        <location line="+12"/>
        <source>gray</source>
        <translation type="unfinished">grau</translation>
    </message>
    <message>
        <location line="-4"/>
        <source>alpha</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>rotated shapes are currently not supported.</source>
        <translation type="obsolete">Das Rotieren der geometischen Formen wird derzeit nicht unterstützt.</translation>
    </message>
    <message>
        <location filename="../../sharedFiles/drawItem.cpp" line="+1052"/>
        <source>invalid geometric shape type</source>
        <translation type="unfinished">Ungültige Typ der geometischen Formen</translation>
    </message>
    <message>
        <source>plot1D</source>
        <translation type="obsolete">Plot1D</translation>
    </message>
    <message>
        <location filename="../itom1DQwtPlotPlugin.cpp" line="+39"/>
        <source>itom widget for 1D dataObjects based on Qwt.</source>
        <translation type="unfinished">itom-Widget für 1D-Datenobjekten basierend auf Qwt.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This designer plugin is an itom widget for linewise / graph-based visualisation of dataObjects and live-outputs of line cameras. This widget is based on the Qwt framework (http://qwt.sf.net).</source>
        <translation type="unfinished">Dieses Designer-Plugin ist ein itom Widget zur zeilenweise/Graphbasierten Visualisierung von DataObjects und Live-Anzeige von Linienkamera. Das Widget basiert auf Qwt framework (http://qwt.sf.net).</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>LGPL, for Qwt see Qwt License</source>
        <translation type="unfinished">LGPL, für Qwt siehe Qwt-Lizenz</translation>
    </message>
    <message>
        <location filename="../dataObjectSeriesData.cpp" line="+198"/>
        <source>cv:Mat in data object seems corrupted</source>
        <translation type="unfinished">cv::Mat im Datenobjekt scheint defekt</translation>
    </message>
    <message>
        <location line="+64"/>
        <source>y-axis</source>
        <translation>y-Achse</translation>
    </message>
    <message>
        <location line="+68"/>
        <source>x-axis</source>
        <translation>x-Achse</translation>
    </message>
    <message>
        <location line="+214"/>
        <location line="+5"/>
        <source>x/y-axis</source>
        <translation>x/y-Achse</translation>
    </message>
    <message>
        <location line="+75"/>
        <source>z-axis</source>
        <translation>z-Achse</translation>
    </message>
    <message>
        <location filename="../plot1DWidget.cpp" line="-988"/>
        <source>Plot1D</source>
        <translation>Plot1D</translation>
    </message>
    <message>
        <location line="+1753"/>
        <source>wrong xData data type. Complex64 and Rgba32 are not supported.</source>
        <translation type="unfinished">Falscher xData-Datentyp. Complex64 und RGBA32 werden nicht unterstützt.</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>recieved a NULL pointer</source>
        <translation type="unfinished">NULL-Pointer übergeben</translation>
    </message>
    <message>
        <location filename="../dataObjectSeriesDataXY.cpp" line="+199"/>
        <source>cv::Mat in data Object representing the xData seems corrupted</source>
        <translation type="unfinished">cv::Mat im Datenobjekt repräsentierendes xData schein defekt</translation>
    </message>
</context>
<context>
    <name>WidgetCurveProperties</name>
    <message>
        <location filename="../widgetCurveProperties.ui" line="+26"/>
        <source>Form</source>
        <translation type="unfinished">Fenster</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Properties</source>
        <translation type="unfinished">Eigenschaften</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Visible</source>
        <translation type="unfinished">Anzeigen</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Legend Visible</source>
        <translation type="unfinished">Legende anzeigen</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Line Width</source>
        <translation type="unfinished">Linienbreite</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Line Style</source>
        <translation type="unfinished">Linienstil</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Line Symbol</source>
        <translation type="unfinished">Liniensymbol</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Symbol Size</source>
        <translation type="unfinished">Symbolgröße</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Line Color</source>
        <translation type="unfinished">Linienfarbe</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Symbol Color</source>
        <translation type="unfinished">Symbolfarbe</translation>
    </message>
</context>
</TS>
